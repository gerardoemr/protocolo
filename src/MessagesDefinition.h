/** @file messagesDefinition.h
 *  @brief Archivo que define los mensajes intercambiados 
 *         entre el cliente y el servidor.
 *
 *  @author Leonardo Hernández
 *  @author Gerardo Martínez
 *  @author Dimitri Semenov
 *  @bug No known bugs.
 */

/** @brief Estructura que contiene los mensajes intercambiados
 *         etre el cliente y el servidor.
 */
typedef struct msgTransaction
{
    unsigned char code[4];    
    unsigned char idPokemon[2];
    unsigned char imageSize[4];     
    unsigned char image[2]; //corregir 
}msgTransaction;
/*

    +--------+-----------+------------+-------+
    |  code  | idPokemon | imageSize  | image |
    +--------+-----------+------------+-------+
      1 byte    1 byte      4 bytes    k bytes

*/

/**
 * Enumeración utilizada para definir los estados del autómata.
 */
enum estados
{   
    S0,
    S1,
    S2,
    S3,
    S4,
    S5,
    S6,
    S7,
    S8,
    S9
};

enum estados estado;

