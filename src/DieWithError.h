#include <stdio.h> /* perror() */
#include <stdlib.h> /* exit() */

void DieWithError(char *errorMessage)
{
    /* perror() produce un mensaje hacia la stderr que es la salida estándar de error*/
    perror(errorMessage);
//exit(1);
}
