/** @file clientPokemon.c
 *  @brief Archivo que define el comportamiento del cliente.
 *
 *  @author Leonardo Hernández
 *  @author Gerardo Martínez
 *  @author Dimitri Semenov
 *  @bug No known bugs.
 */

/* -- Includes -- */
#include <stdio.h>          /* printf(), fprint() */
#include <sys/socket.h>     /* socket(), connect(), send(), recv() */
#include <arpa/inet.h>      /* sockaddr_in, inet_addr() */
#include <stdlib.h>         /* atoi() */
#include <string.h>         /* memset() */
#include <unistd.h>         /* close(), sleep() */
#include "DieWithError.h"   /* DieWithError() */

#include "MessagesDefinition.h"
#include "consultas.h"

#define RCVBUFSIZE 22 /* Tamaño de receive buffer en bytes */

/**
 * Devuelve el primer caracter de la entrada estándar e ignora todo hasta fin de
 * línea o fin de archivo, limpiando el buffer.
 */
char clean_getchar() {
	char leido = getchar();
	int c;
	while((c = getchar()) != '\n' && c != EOF);
	return leido;
}


/** @brief Función para el manejo de errores.
 *
 *  La función detecta errores al ejecutar connect(), send(), recv() etc.
 *
 *  @param errorMesage mensaje de error a imprimir
 *  @return Void.
 */
void DieWithError(char *errorMessage);


/** @brief Función main del cliente.
 *
 *
 *  @param
 * 
 *  @return
 *
 */
int main(int arg, char *argv[])
{
    int sock;                           /* Socket descriptor */
    struct sockaddr_in echoServAddr;    /* Echo server address */
    unsigned short servPort;            /* Echo server port */
    char *servIP;                       /* Server IP address (dotted quad) */
    char *echoString;                   /* String to send to echo server */
    char echoBuffer[RCVBUFSIZE];        /* Buffer for echo string */
    unsigned int echoStringLen;         /* Length of string to echo */
    int bytesRcvd, bytesSend;           /* Bytes read in single recv() and bytes sent */

    char ans;                       /* variable para guardar las respuestas del usuario.*/

    unsigned char code, imageSize;
    unsigned char idPokemon, image;

    char *n;  /* nombre del pokemon como string*/

    if ((arg != 7)) /* Verifica que el número de argumentos sea correcto. */
    {
        fprintf(stderr, "Uso: %s <IP Servidor> <Puerto> <Code> <idPokemon> <imageSize> <image>\n ",argv[0]);
        exit(1);
    }

    servIP = argv[1];           /* First arg: server IP address (dotted quad) */
    servPort = atoi(argv[2]);   /* Use given port. Cast string to integer */
    code = atoi(argv[3]);
    idPokemon = atoi(argv[4]);
    imageSize = atoi(argv[5]);
    image = atoi(argv[6]);

    /* Create a struct type of msgTransaction: reserve memory and cast to msgTransaction type */
    msgTransaction *msgTran = (msgTransaction *) malloc(sizeof(msgTransaction));

    /* Copy values from user input to msgTran */
    memcpy(msgTran->code,      &code,      sizeof(msgTran->code));
    //memcpy(msgTran->idPokemon, &idPokemon, sizeof(msgTran->idPokemon));
    memcpy(msgTran->imageSize, &imageSize, sizeof(msgTran->imageSize));
    memcpy(msgTran->image,     &image,     sizeof(msgTran->image));

    /**
     *  Create a reliable stream socket using TCP.
     *  int socket(int protocolFamily, int type, int protocol)
     *  el primer parametro determina la familia del protocolo del socket, con la API del socket
     *  se pueden usar diferentes familias de protocolos, aquí se usa la familia del protocolo
     *  de Internet, valor PF_INET o AF_INET. El segundo parametro espeifica el tipo de socket,
     *  SOCK_STREAM para sockets orientados a conexion, SOCK_DGRAM ara sockets no orientados a conexion.
     *  El tercer parametro, el protocolo de transporte a usar de punto a punto (host a host),
     *  IPPROTO_TCP o IPPROTO_UDP. El valor de retorno de la funcion es un entero que es tratado como un
     *  descirptor de archivo, llamado descriptor del socket, este se usa en otras funciones para
     *  identificar al socket creado.
     **/
    if((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        DieWithError(" socket() failed");  /* Un valor de -1 indica una falla en la creación del socket */
        return 1;
    }

    /* Construct the server addres structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr)); /* Zero out structure
                                                       Se asegura que los bytes extras de la estructura que
                                                       no son usados sean cero. No es necesario para
                                                       todos los sistemas */

    echoServAddr.sin_family      = AF_INET;         /* Internet addres family */

    echoServAddr.sin_addr.s_addr = inet_addr(servIP);    /* Server IP address
                                                            inet_addr convierte la cadena de dir IP, está en
                                                            forma de 4 octetos de bits separados por punto, a 32 bits */

    echoServAddr.sin_port       = htons(servPort); /* Server port */
                                                   /* htons convierte a formato de red el puerto, convierte a la
                                                   ordenacion big-endian los bytes que corresponden al puerto */

    /* Establish the connection to the echo server */
    if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0) {
        DieWithError("connect () failed");
        return 1;
    }

    //LOOP
    int fin = 0;        /* variable auxiliar para identificar el fin de conexión */
    int error = 0;      /* variable para identificar el último estado visitado */
    estado = S0;

    while (fin == 0){
        //printf("estado, code, error: %i, %i , %i\n",estado,code,error);
        memcpy(msgTran->idPokemon, &idPokemon,sizeof(msgTran->idPokemon));

        switch(estado){

            case S0:    // Cliente solicita un pokemon al servidor
                code = 10;
                memcpy(msgTran->code, &code,sizeof(msgTran->code));
                estado = S1;
                break;

            case S1: //Servidor envió un pokemon
                printf("El servidor envió un Pokemon\n");
                if (code == 20)
                    estado = S2;
                break;

            case S2:    // Servidor pregunta al cliente si desea intentar capturar al Pokemon.
                ;
                n = getName(idPokemon);
                printf("¿Intentar capturar a %s ? (S/N)\n ",n);
                ans = clean_getchar();

                if ( ans == 's' || ans == 'S' )
                    code = 30;
                else
                    code = 31;

                if (code == 30)  //El cliente intenta capturar al Pokemon.
                    estado = S3;
                if (code == 31)
                    estado = S5; //El cliente rechazó la opción de Pokemon.

                memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                error = 2;
                break;

            case S3: //Servidor informa del estado de la captura.
                if (code == 23){ //Servidor informa que el cliente no tiene más intentos
                    printf("No hay más intentos de captura\n");
                    estado = S5;
                } else if (code == 21){ //Servidor pregunta al cliente si desea intentar capturar al Pokemon de nuevo.
                    estado = S4;
                    printf("No se capturó al Pokemon\n");
                } else if (code == 22){  //Servidor informa que el Pokemon fue capturado (enviar imagen)
                    estado = S5;
                    printf("Pokemon capturado!\n");
                    updatePokedex(idPokemon);
                } else
                    estado = S3;

                //memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                error = 3;
                break;

            case S4: //Cliente informa si desea continuar el intento de captura.
                
                n = getName(idPokemon);
                printf("¿Intentar capturar a %s de nuevo? (S/N)\n ",n);
                ans = clean_getchar();

                if ( ans == 'S' || ans == 's' ) //El cliente decide realizar otro intento.
                    code = 30;
                else           // El cliente decide abortar.
                    code = 31;
                if (code == 30)
                    estado = S3;
                else if (code == 31)
                    estado = S5;

                memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                error = 4;
                break;

            case S5:    // El servidor pregunta al cliente si desea continuar.
                printf("¿Deseas continuar jugando? (S/N)\n");
                ans = clean_getchar();

                if( ans == 's' || ans == 'S')
                    code = 30;
                else
                    code = 31;
                if (code == 30)
                    estado = S1;
                else
                    estado = S6;
                memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                error = 5;
                break;

            case S6: //El cliente decidió terminar el juego.
                fin = 1;
                code = 32;
                memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                error = 6;
                break;

            default:
                printf("last code: %i \n",code);
                printf("last state: %i \n", error);
                break;
        }

        echoStringLen = sizeof(msgTransaction); /* Determine struct length */
        int message_len = sizeof(msgTransaction);

        // Envía mensaje al servidor
        bytesSend = send(sock, msgTran, message_len,0);
        if (bytesSend != echoStringLen) {
            DieWithError("send(), error: numero de bytes distinto al esperado\n");
            return 1;
        }

        /* Receive up to the buffer size (minus 1 to leave space for a null terminator) bytes from the sender */
        bytesRcvd = recv(sock, echoBuffer, sizeof(echoBuffer), 0);
        if (bytesRcvd < 0) {
            DieWithError("recv() un error ocurrio o la conexion se cerro antes de tiempo.");
            return 1;
        }

        /* Cast echoBuffer (with received data to msgTransaction structure) */
        msgTransaction *msgTran = (struct msgTransaction *)echoBuffer;

        /* Cast members of structure to integer (member is a pointer to array of char */
        code      = *(msgTran->code);
        idPokemon = *(msgTran->idPokemon);
        imageSize = *(msgTran->imageSize);
        image     = *(msgTran->image);

        //Simular retraso por usuario o red lenta.
        sleep(1);
    }

    // Escribir pokemones en el directorio del cliente. writeToFile(int id) y ajustar errores de lectura/escritura
    printf("Guardando tu pokemon capturado! \n");
    writeToFile(1);


    /* Termina la conexión. Esta funcion le dice a los protocolos de la pila TCP/IP que inicien las acciones
     necesarias para terminar las comunicaciones y liberar cualquier recurso que este asociado al socket.
     Ya no será posible enviar o recibir datos a través de este socket cerrado. */
    close(sock);

    exit(0);

}
