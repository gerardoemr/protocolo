/** @file consultas.h
 *  @brief Archivo que define las funciones que permiten
 *         realizar consultas a la base de datos. 
 *  
 *  La base de datos se encuentra en un servidor y contiene
 *  las siguientes relaciones.
 * 
 *  generationOne (pokedex_number, named, image, ...)
 *  int pokedex_number := primary key
 *  text named := nombre del pokemon
 *  bytea a := representación en bytes de la imagen del pokemon. (formato PNG) 
 *  
 *  La relación anterior tiene otros campos con información adicional del pokemon
 *  en nuestra implementación solo se utilizaron estos tres atributos. 
 * 
 *  entrenador( id, nombre )
 *  int id := primary key
 *  text nombre := nombre del entrenador
 *  
 *  pokedex(fk_pokemon, fk_entrenador)
 *  fk_pokemon := referencia a la tabla generationOne(pokedex_number)
 *  fk_entrenador := referencia a la tabla entrenador(id)
 * 
 *  Se utilizó postgresql como manejador para la base de datos.
 *  El contenido de la base se tomó de www.kaggle.com 
 *  https://www.kaggle.com/rounakbanik/pokemon/version/1#_=_
 *  https://www.kaggle.com/kvpratama/pokemon-images-dataset
 *  
 *  @author Leonardo Hernández
 *  @author Gerardo Martínez
 *  @author Dimitri Semenov
 *  @bug No known bugs.
 */


#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include "postgresql/libpq-fe.h"  /* Biblioteca con funciones para comunicación con la base de datos. */

/* ntohl/htonl */
#include <netinet/in.h>
#include <arpa/inet.h>

/**
 * Estructura que contiene como primer miembro el tamaño de la imagen
 * y como segundo la representación en bytes de la imagen.
 */
typedef struct imagen{
    int  imgSize;
    char *img;
}imagen;

/**
 * Función para terminar las conexiones a la base de datos en caso de error. 
 */
static void
exit_nicely(PGconn *conn)
{
    PQfinish(conn);
    exit(1);
}

/**
 * @param id Identificador del entrenador pokemon.
 * 
 * @return struct imagen Estructura que contiene el tamaño de la imagen 
 *                       y su representación en bytes.
 */
struct imagen
getImage(int id)
{
    const char *conninfo;         /*variable que contiene los parámetros para conexión a la base de datos*/  
    PGconn     *conn;             /*variable que tiene la conexión */
    PGresult   *res;              /* resultado de consultas */ 
    const char *paramValues[1];   /* variables auxiliares para realizar las consultas */
    int         paramLengths[1];
    int         paramFormats[1];
    uint32_t    binaryIntVal;
    struct imagen resultado;      /* estrctura donde se guardará el resultado */

    /*
     * Parámetros para la conexión a la base de datos. 
     */
    conninfo = "dbname = pokemones hostaddr=34.218.152.58 port=5432 user=client password=Cl13nT";

    /* Realiza la conexión a la base de datos. */
    conn = PQconnectdb(conninfo);

    /* Revisa que la conexión al backend se haya realizado de manera correcta */
    if (PQstatus(conn) != CONNECTION_OK)
    {
        fprintf(stderr, "Connection to database failed: %s",
                PQerrorMessage(conn));
        exit_nicely(conn);
    }

    /**
     * Se especifica una ruta de búsqueda segura para evitar que terceros tomen el control.
     * Recomendado por la documentación de PostgreSQL
     */
    res = PQexec(conn, "SET search_path = public");
    if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        fprintf(stderr, "SET failed: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }
    PQclear(res);

    /*
     * Se transmite el parámetro de búsqueda id en su representación binaria.
     * 
     * Aunque se especifican varios parámetros se deja que el backend
     * deduzca el tipo de dato del parámetro, la decisión "se fuerza" cuando
     * se hace cast del símbolo del parámetro en el texto de la consulta.
     * Es una medida de seguridad recomendada por PostgreSQL cuando se envían
     * parámetros binarios.
     * 
     */

    /* Se convierte el parámetro id a su representación binaria */
    binaryIntVal = htonl((uint32_t) id);

    /* Se definen los parámetros para PQexecParams */
    paramValues[0] = (char *) &binaryIntVal;
    paramLengths[0] = sizeof(binaryIntVal);
    paramFormats[0] = 1;        /* binary */

    /* Se realiza la consulta */
    res = PQexecParams(conn,
                       "SELECT * FROM generationone WHERE pokedex_number = $1::int4",
                       1,       /* one param */
                       NULL,    /* let the backend deduce param type */
                       paramValues,
                       paramLengths,
                       paramFormats,
                       1);      /* ask for binary results */

    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        fprintf(stderr, "SELECT failed: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }

    int         i, // variable auxiliar para iterar sobre tuplas de la tabla obtenida
                j; // variable auxiliar para iterar los bytes de la imagen
    int         t_fnum, //named, atributo de la tabla
                b_fnum; //image, atributo de la tabla

    /* PQfnumber para evitar suposiciones sobre el orden de los campos en el resultado de la consulta. */
    t_fnum = PQfnumber(res, "named"); //nombre del Pokemon
    b_fnum = PQfnumber(res, "image"); //imagen del Pokemon

    /* Se iteran los resultados de la consulta. */
    for (i = 0; i < PQntuples(res); i++)
    {
        char       *tptr;
        char       *bptr;
        int         blen;
        int         ival; 

        /* Se obtienen los valores de las celdas (no se considera el caso de resultado NULL) */
        tptr = PQgetvalue(res, i, t_fnum);
        bptr = PQgetvalue(res, i, b_fnum);

        /* Longitud (en bytes) de la representación binaria de la imagen */
        blen = PQgetlength(res, i, b_fnum);

        resultado.imgSize = blen;
        resultado.img =  bptr;
    }

    // Se limpian los resultados de la consulta
    PQclear(res);

    // Se cierra la conexión a la base de datos y se limpia memoria.
    PQfinish(conn);

    return resultado;
}

/**
 * Función que recibe el id de un pokemon y devuelve su nombre.
 * 
 * @param id Identificador de pokemon en la tabla generationOne(pokedex_number)
 * 
 * @return char* nombre Cadena con el nombre del pokemon.
 * 
 */
char* getName(int id)
{
    const char *conninfo;
    PGconn     *conn;
    PGresult   *res;
    const char *paramValues[1];
    int         paramLengths[1];
    int         paramFormats[1];
    uint32_t    binaryIntVal;
    int len;
    char *nombre;

    /*
     * Parámetros para la conexión a la base de datos. 
     */
    conninfo = "dbname = pokemones hostaddr=34.218.152.58 port=5432 user=client password=Cl13nT";

    /* Realiza la conexión a la base de datos. */
    conn = PQconnectdb(conninfo);

    /* Se segura que la conexión con el backend se haya realizado de manera correcta. */
    if (PQstatus(conn) != CONNECTION_OK)
    {
        fprintf(stderr, "Connection to database failed: %s",
                PQerrorMessage(conn));
        exit_nicely(conn);
    }

    /* Establece ruta segura de búsqueda */
    res = PQexec(conn, "SET search_path = public");
    if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        fprintf(stderr, "SET failed: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }
    PQclear(res);

    /* Representación binaria del parámetro de búsqueda */
    binaryIntVal = htonl((uint32_t) id);

    /* Se definen parámetros para PQexecParams */
    paramValues[0] = (char *) &binaryIntVal;
    paramLengths[0] = sizeof(binaryIntVal);
    paramFormats[0] = 1;        

    /* Se realiza la consulta */
    res = PQexecParams(conn,
                       "SELECT * FROM generationone WHERE pokedex_number = $1::int4",
                       1,       /* un parámetro */
                       NULL,    /* deja al backend el trabajo de deducir el tipo */
                       paramValues,
                       paramLengths,
                       paramFormats,
                       1);      /* se piden los resultados con formato binario */

    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        fprintf(stderr, "SELECT failed: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }

    int         i,      //variable auxiliar para iterar los resultados.
                j;      // variable auxiliar para iterar sobre los bytes de la imagen.
    int         t_fnum, //named
                b_fnum; //image 

    /* PQfnumber evita suposiciones sobre el orden de los campos en los resultados de la consulta */
    t_fnum = PQfnumber(res, "named"); //nombre del Pokemon
    b_fnum = PQfnumber(res, "image"); //imagen del Pokemon

    //Se iteran los resultados
    for (i = 0; i < PQntuples(res); i++)
    {
        char       *tptr;
        char       *bptr;
        int         blen;
        int         ival; 

        /* Se obtienen los valores de las celdas. (No se considera el caso de que sean NULL) */
        tptr = PQgetvalue(res, i, t_fnum);
        bptr = PQgetvalue(res, i, b_fnum);

        // longitud de la representación binaria de la imagen
        len = PQgetlength(res, i, t_fnum); 
        
        nombre = tptr;
    }

    //Se limpian los resultados de la consulta
    PQclear(res);

    /* Cierra la conexión limpia memoria. */
    PQfinish(conn);

    return nombre;
}

/**
 * @brief Función que escribe las imágenes de pokemones en el directorio donde se ejecuta.
 * 
 * Los resultados se obtiene apartir de un inner join entre las tablas pokedex y generationOne.
 * En el resultado se muestra id, nombre e imagen del pokemon. para cada pokemon capturado por
 * un entrenador en particular.
 *  
 * @param id identificador del entrenador con el que se obtiene los pokemones capturados.
 * 
 * @return 0 en caso de que se ejecute correctamente, -1 en otro caso.
 * 
 */
int writeToFile(int id){
    const char *conninfo;
    PGconn     *conn;
    PGresult   *res;
    const char *paramValues[1];
    int         paramLengths[1];
    int         paramFormats[1];
    uint32_t    binaryIntVal;

    conninfo = "dbname = pokemones hostaddr=34.218.152.58 port=5432 user=client password=Cl13nT";

    /* Realiza conexión a la base de datos. */
    conn = PQconnectdb(conninfo);

    /* Se revisa que la conexión al backend se haya realizado de manera correcta. */
    if (PQstatus(conn) != CONNECTION_OK)
    {
        fprintf(stderr, "Connection to database failed: %s",
                PQerrorMessage(conn));
        exit_nicely(conn);
    }

    /* Se establece una ruta de búsqueda segura. */
    res = PQexec(conn, "SET search_path = public");
    if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        fprintf(stderr, "SET failed: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }
    PQclear(res);

    /* Representación binaria del parámetro de búsqueda. */
    binaryIntVal = htonl((uint32_t) id);

    /* parámetros para PQexecParams */
    paramValues[0] = (char *) &binaryIntVal;
    paramLengths[0] = sizeof(binaryIntVal);
    paramFormats[0] = 1;        /* binary */

    /* Se realiza la consulta */
    res = PQexecParams(conn,
                       "SELECT  pokedex.fk_pokemon, generationOne.named, generationOne.image FROM pokedex INNER JOIN generationOne ON pokedex.fk_pokemon = generationone.pokedex_number AND pokedex.fk_entrenador = $1::int4",
                       1,       /* un parámetro */
                       NULL,    /* backend deduce el tipo del parámetro */
                       paramValues,
                       paramLengths,
                       paramFormats,
                       1);      /* se solicitan resultados en formato binario */

    if (PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        fprintf(stderr, "SELECT failed: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }

    int         i,      // variable para iterar los resultados
                j,      // variable para iterar los bytes de la imagen.
                blen;   // longitud (en bytes) de la representación binaria de la imagen
    int         i_fnum, // id_pokemon
                t_fnum, // nombre
                b_fnum; // imagen 

    /*PQfnumber para evitar suposiciones en el orden de los campos del resultado. */
    i_fnum = PQfnumber(res, "fk_pokemon");
    t_fnum = PQfnumber(res, "named");
    b_fnum = PQfnumber(res, "image");

    // Se iteran resultados
    for (i = 0; i < PQntuples(res); i++)
    {

        char       *iptr;
        char       *tptr;
        char       *bptr;
        int         ival; 

        /* Se obtienen los valores de cada celda. (Se supone que son diferentes de NULL) */
        iptr = PQgetvalue(res, i, i_fnum);
        tptr = PQgetvalue(res, i, t_fnum);
        bptr = PQgetvalue(res, i, b_fnum);

        /* Rep. binaria del parámetro de búsqueda*/
        ival = ntohl(*((uint32_t *) iptr));

        /* Longitud de la imagen en bytes */
        blen = PQgetlength(res, i, b_fnum);

        /*Bloque de pruebas*/
        // printf("tuple %d: got\n", i);
        // printf(" id = (%d bytes) %d\n",
        //        PQgetlength(res, i, i_fnum), ival);
        // printf(" nombre = (%d bytes) '%s'\n",
        //        PQgetlength(res, i, t_fnum), tptr);
        // printf(" imagen = (%d bytes) ", blen);
        // printf("\n\n");
        
    
        /* El siguiente bloque se encarga de guardar la imagen */

        FILE *fptr;

        char dest[100];

        //Estas dos lineas sirven para darle nombre al archivo
        memset(dest, '\0', sizeof(dest));
        strncpy(dest, tptr, sizeof(tptr));
        
        //Para concatenar el formato (no creo que sea necesario)
        //strcat(dest, ".png");
        
        //Verificar si la imagen ya existe
        if( access( dest , F_OK ) != -1 ) {
            // la imagen ya existe, no debemos sobreescribir. 
        } else {
            // la imagen no existe
            if ((fptr = fopen(dest,"wb")) == NULL){
                printf("Error! opening file");
                return -1;
            }
            fwrite(bptr, 1, blen, fptr);
            fclose(fptr);
        }

    }

    /* Se limpian los resultados de la consulta */
    PQclear(res);

    /* Cierra la conexión con la base de datos y realiza limpieza. */
    PQfinish(conn);

   return 0;
}


/**
 * @brief función que devuelve 
 */
int updatePokedex(int id){
    
    const char *conninfo;
    PGconn     *conn;
    PGresult   *res;
    const char *paramValues[1];
    int         paramLengths[1];
    int         paramFormats[1];
    uint32_t    binaryIntVal;

    conninfo = "dbname = pokemones hostaddr=34.218.152.58 port=5432 user=client password=Cl13nT";

    /* Realiza conexión a la base de datos. */
    conn = PQconnectdb(conninfo);

    /* Se revisa que la conexión al backend se haya realizado de manera correcta. */
    if (PQstatus(conn) != CONNECTION_OK)
    {
        fprintf(stderr, "Connection to database failed: %s",
                PQerrorMessage(conn));
        exit_nicely(conn);
    }

    /* Se establece una ruta de búsqueda segura. */
    res = PQexec(conn, "SET search_path = public");
    if (PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        fprintf(stderr, "SET failed: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }
    PQclear(res);

    char str[12];
    sprintf(str, "%d", id); /* Cast de int a string para facilitar consulta */

    paramValues[0] = str;

    res = PQexecParams(conn,
                       "INSERT INTO pokedex (fk_entrenador,fk_pokemon) values (1, $1 )",
                       1,       /* one param */
                       NULL,    /* let the backend deduce param type */
                       paramValues,
                       NULL,    /* don't need param lengths since text */
                       NULL,    /* default to all text params */
                       1);      /* ask for binary results */

    // if (PQresultStatus(res) != PGRES_TUPLES_OK)
    // {
    //     //fprintf(stderr, "SELECT failed: %s", PQerrorMessage(conn));
    //     PQclear(res);
    //     exit_nicely(conn);
    // }

    /* Se limpian los resultados de la consulta */
    PQclear(res);

    /* Cierra la conexión con la base de datos y realiza limpieza. */
    PQfinish(conn);

   return 0;

}