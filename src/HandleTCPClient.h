/** @file clientPokemon.c
 *  @brief Archivo para manjear conexiones TCP.
 *
 *  La función principal HandleTCPClient() realiza las transiciones
 *  de estados del lado del servidor y se encarga del envío y
 *  recepción de mensajes con el cliente.
 *
 *  @author Leonardo Hernández
 *  @author Gerardo Martínez
 *  @author Dimitri Semenov
 *  @bug No known bugs.
 */

#include <stdio.h>              /* printf() y fprintf()*/
#include <sys/socket.h>         /* recv() y send() */
#include <unistd.h>             /* close() */
#include <string.h>             /* strcpy() */
#include "MessagesDefinition.h"

#define RCVBUFSIZE 32                  /* tamaño de receive buffer */
#define SOCKET_READ_TIMEOUT_SEC 10

void DieWithError(char *errorMessage); /* Error handling function*/

/**
 *  Función principal
 */
void *HandleTCPClient(void *clientSocket)
{
    int clntSocket = (int) clientSocket;        /* Especifica el socket por el cual se comunican cliente y servidor.*/
    char echoBuffer[RCVBUFSIZE];                /* Buffer para echo string */
    int recvMsgSize;                            /* tamaño del mensaje recibido */

    unsigned char code, imageSize;              /* Miembros del struct msgTran definido en el archivo "MessagesDefinition.h" */
    unsigned char idPokemon, image;

    int s = clntSocket;                         /* variables auxiliares */
    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(s, &rfds);
    char serverResponse[22];    /* arreglo de tipo char para guardar mensaje de confirmación del servidor. */

    strcpy(serverResponse, "Data received");

    estado = S0;        /* Estado inicial */
    int fin = 0;        /* variable auxiliar para cerrar la conexión */
    int intentos = 0;   /* variables auxiliares para capturar Pokemon */
    int captura = 0;
    int error = 0;      /* variable para identificar el último estado visitado */
    int r;

    /**
     * El siguiente ciclo se mantiene activo hasta que el cliente decida terminar la conexión
     * o hasta que ocurra un timeout.
     */
    while (fin == 0){
        captura = 0;
        struct timeval timeout;                     /* estructura para implementar timeout */
        timeout.tv_sec = SOCKET_READ_TIMEOUT_SEC;
        int rv = select(s + 1, &rfds, (fd_set *) 0, (fd_set *) 0, &timeout);
        if(rv <= 0) {
             printf("Timeout!\n");
             return (void *) 1;
        }


        /* Recibimos mensaje del cliente */
        if ((recvMsgSize = recv(clntSocket, echoBuffer, RCVBUFSIZE, 0)) < 0)
            DieWithError("recv() falló\n");

        /* Convertimos el mensaje recibido en echoBuufer a un struct msgTran */
        msgTransaction *msgTran = (struct msgTransaction *)echoBuffer;

        /* Convertimos cada miembro del struct a int. */
        code      = *(msgTran->code);
        idPokemon = *(msgTran->idPokemon);
        imageSize = *(msgTran->imageSize);
        image     = *(msgTran->image);

        /* Se imprime el mensaje de cliente. */
        printf("code: %i \n",code);
        printf("idPokemon: %i \n",idPokemon);
        printf("imageSize: %i \n",imageSize);
        printf("image: %i \n",image);
        printf("estado, code, error: %i, %i , %i\n",estado,code,error);
        printf("------------------\n");

        /**
         * En el siguiente bloque de código se implementa el autómata descrito
         * en la sección 2 del reporte. Por practicidad se omite el estado 7,
         * en el cual servidor y cliente terminan la conexión, simplemente el estado 6
         * envía la señal para salir del while y luego se cierra la conexión.
         */
        switch(estado){

            case S0: //Cliente solicitó un pokemon
                if (code == 10)
                    estado = S1;
                break;

            case S1:    //El servidor envía un pokemon aleatorio al cliente.
                code = 20;

                r = (rand() % 151) + 1;   //número aleatorio en [1,151]

                idPokemon = r;

                printf("%i",idPokemon);

                memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                memcpy(msgTran->idPokemon, &idPokemon,sizeof(msgTran->idPokemon));
                estado = S2;

                error = 1;

                break;

            case S2:  // Cliente elige una opción.
                if (code == 30) //El cliente intenta capturar al Pokemon.
                    estado = S3;
                if (code == 31)
                    estado = S5; //El cliente rechazó al Pokemon.

                memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                error = 2;
                break;

            case S3: // Servidor determina si el pokemon es capturado.
                r = (rand() % 10) + 1; // aleatorio en [1,10]

                if (r < 3){
                    captura = 1;
                    intentos = 0;
                }
                else
                    intentos += 1;
            
                if (captura){
                    code = 22;
                    intentos = 0;
                }
                else if (intentos >= 3){
                    code = 23;
                    intentos = 0;
                }
                else
                    code = 21;

                if (code == 21)
                    estado = S4;
                if (code == 22 || code == 23)
                    estado = S5;

                memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                error = 3;
                break;

            case S4: //Cliente informa si desea continuar el intento de captura.
                if (code == 30) //El cliente decide realizar otro intento.
                    estado = S3;
                if (code == 31)
                    estado = S5; // El cliente decide abortar.

                memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                error = 4;

                break;

            case S5:    // El servidor pregunta al cliente si desea continuar jugando.
                if (code == 30) // El cliente decide seguir el juego.
                    estado = S1;
                if (code >= 31)
                    estado = S6;

                memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                error = 5;

                break;

            case S6:    //Estado para cerrar el socket.
                fin = 1;
                code = 32;
                memcpy(msgTran->code,      &code,     sizeof(msgTran->code));
                error = 6;
                break;

            default:
                printf("last code : %i\n",code);
                printf("last state: %i\n",error);
                break;
        }
        /* Respuesta al cliente */
        if(fin != 1) {
        int test = send(clntSocket, msgTran, sizeof(serverResponse), 0);
        if(test < 0) {
            DieWithError("send() failed\n");
            return (void *) 1;
        }
       }

    }
    //Termina ciclo while

    printf("Socket cerrado\n");
    close(clntSocket); /* Cierra el socket del cliente*/
}
