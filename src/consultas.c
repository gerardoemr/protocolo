/** @brief Archivo para probar que las consultas se ejecuten
 *         de manera correcta. 
 * 
 * Se imprime el resultado de las consultas obtenidas
 * por getImage(int id) y getName(int id). 
 * 
 * La consulta writeToFile(int id) escribe imágenes en el mismo
 * directorio donde se ejecuta el archivo.
 * 
 * compilación: gcc consultas.c -o consultas -lpq 
 * uso:         ./consultas 0.0.0.0 9999 1 2 3 4
 *  
 * @author Leonardo Hernández
 * @author Gerardo Martínez
 * @author Dimitri Semenov
 * @bug No known bugs.
 * 
 */ 

#include "consultas.h"

int main(){

    int x = 5; //el valor del identificador a buscar debe estar en [1,151]

    printf("getImage with id %i\n",x);
    
    struct imagen a = getImage(x);

    int j;

    printf("Número de bytes: %i \n",a.imgSize);

    // for (j = 0; j < a.imgSize; j++)
    //         printf("\\%03o", a.img[j]);
    //     printf("\n\n");

    //getName...
    
    printf("getName with id %i\n",x);
    
    char *nom = getName(x);

    int len;

    len = sizeof(nom);

    printf("Número de bytes: %i \n",len);

    printf("%s", nom);
    printf("\n\n");

    //writeToFile...

    printf("================\n\n");

    printf("Prueba writeToFile(int id)\n Se muestran las tuplas de los pokemones capturados por el entrenador\n\n");

    writeToFile(1);

    updatePokedex(100);


    return 0;
};