/** @file serverPokemon.c
 *  @brief Archivo que define el comportamiento del servidor.
 *
 *  Este archivo se encarga de configurar el socket y los
 *  parámetros necesarios para establecer la comunicación
 *  con el cliente. Cuando un cliente solicita conexión,
 *  el servidor la establece y delega el manejo de esta a
 *  la función handleTCPClient().
 *
 *  compilación: gcc serverPokemon.c -o serverPokemon
 *  uso:         ./serverPokemon 9999
 *
 *  @author Leonardo Hernández
 *  @author Gerardo Martínez
 *  @author Dimitri Semenov
 *  @bug No known bugs.
 */

#include <stdio.h>              /* printf() y fprintf()*/
#include <sys/socket.h>         /* socket(), bind(), connect() */
#include <arpa/inet.h>          /* sockaddr_in, inet_ntoa() */
#include <stdlib.h>             /* atoi() */
#include <string.h>             /* memset() */
#include <unistd.h>             /* close() */
#include <signal.h>             /* Manejo de señales */

#include "pthread.h"            /* manejo de hilos */

#include "DieWithError.h"       /* impresión y manejo de errores */

#include "HandleTCPClient.h"    /* manejo de la conexión con el cliente. */

#define MAXPENDING 5            /* Número máximo de conexiones */

void DieWithError(char *errorMessage);

void *HandleTCPClient(void *clntSocket);

/**
 *
 * Se
 *
 */
int main(int argc, char *argv[])
{
    signal(SIGPIPE, SIG_IGN);           /* Cambio de manejo de sigpipe */
    int servSock;                       /* Descriptor del socket para el server */
    struct sockaddr_in echoServAddr;    /* dirección local */
    unsigned short echoServPort;        /* Puerto del servido */
    unsigned int clntLen;               /* Longitud de la estructura de datos para la dirección del cliente. */

    if (argc != 2) /* Verifica que el número de argumentos sea el correcto */
    {
        fprintf(stderr, "Uso: %s <Puerto Server>\n",argv[0]);
        exit(1);
    }

    echoServPort = atoi(argv[1]);       /* primer argumento: puerto local. Se convierte de cadena a entero */

    /*Se crea un socket para las conexiones entrantes.*/
    if((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0 ) {
        DieWithError( "socket () failed\n");
        return 1;
    }

    /* Se construye la estructura para direcciones locales */
    memset(&echoServAddr,0,sizeof(echoServAddr));       /* Zero out structure */
                                                        /* Se asegura que los bytes extra de la estructura
                                                           que no son usados sean cero. No es necesario
                                                           para todos los sistemas. */
    echoServAddr.sin_family      = AF_INET;              /* Internet Address Family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY);   /* Cualquier interfaz entrante. Toma el valor de la dir IP
                                                           de la interfaz de red con mayor prioridad en el
                                                           servidor. htonl convierte a ordenación big-endian */

    echoServAddr.sin_port        = htons(echoServPort);  /* Puerto local.
                                                           htons convierte a formato de red el puerto,
                                                           convierte a la ordenación big-endian los
                                                           bytes que corresponen al puerto */

    /* Bind a la dirección local */
    /* Permite asociar una dirección IP y puerto al socket para después usarlo como un canal para
       acpetar a otros sockets que estarán conectando con los clientes. El primer parámetro socket,
       es el descriptor creado por la función socket(). El segundo parámetro es un apuntador a la
       estructura que contiene la dir IP y puerto propios (los del servidor), es necesario hacer
       la conversión (o cast) de la estructura particular de la familia de internet sockaddr_in, a
       la estructura genérica sockaddr. El tercer parámetro es el tamaño de la estructura del segundo
       parámetro, se obtiene con la función sizeof().  */
    if (bind(servSock, (struct sockaddr *)&echoServAddr,sizeof(echoServAddr)) < 0) {
        DieWithError("bind() failed\n");
        return 1;
    }

    /**
     * Habilitamos al socket para que escuche peticiones de conexión.
     *
     * int listen(int socket, int queueLimit)
     * Informa a la implementación de TCP del SO que el servidor está listo para recibir
     * conexiones de los clientes. listen() provoca cambios internos en el socket, cada solicitud de
     * conexión entrante debe ser atendida y puesta en la cola para ser atendida por el programa.
     * La máxima cantidad de conexiones en espera en la cola está dado por el segundo parámetro
     * queueLimit, este valor dependerá de las especificaciones técnicas del equipo en donde se
     * ejecute el servidor.
     **/
    if (listen(servSock, MAXPENDING) < 0) {
        DieWithError("listen() failed\n");
        return 1;
    }
    for (;;) /* Recibe iterativamente conexiones de los clientes. */
    {
        struct sockaddr_in echoClntAddr;    /* Dirección del cliente */
        int clntSock;                       /* Descriptor del socket del cliente */

        /* Establece la longitud del parámetro de salida. */
        clntLen = sizeof(echoClntAddr);

        /** Se espera a la conexión de un cliente.
         *
         * El socket en el servidor en el que ha sido llamada la función listen() actúa de forma
         * diferente a un socket del cliente, en vez de enviar y recibir datos en el socket el
         * servidor llama a la función accept(), esta función saca de la cola a los clientes en
         * espera de conexión con el servidor, si la cola está vacía espera hasta que una
         * solicitud de conexión llegue. El primer parámetro corresponde al descriptor del socket
         * del servidor. accept() coloca en el segundo parámetro que recibe, los valores de la
         * dir IP y puerto del cliente. En el tercer parámetro está el tamaño de los bytes  de
         * estructura del segundo parámetro. accept() regresa un descriptor para un nuevo socket,
         * el cual es el que está conectado al socket del cliente.
         **/
        if ((clntSock = accept(servSock, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0 )
            DieWithError("accept() failed\n");


        /* Conexión con cliente a través del socket clntSock */
        printf("---------------------------\n");
        printf("Atendiendo al cliente: %s: %i\n",
        inet_ntoa(echoClntAddr.sin_addr),   /* Imprime dir IP del cliente, inet_ ntoa()
                                               de representación de 32 bits a formato
                                               de octetos. */
        ntohs(echoClntAddr.sin_port)        /* imprime el puerto del cliente, ntohs() de
                                               ordenación de red (big-endian) a
                                               little-endian.
                                            */

        );

        /**
         *  En esta función se encuetra el comportamiento del servidor recibe como parámetro
         *  el descriptor del socket que ya tiene establecida la conexión con el cliente.
         **/
        printf("Creando hilo de ejecución para atender petición\n");
        pthread_t thread;
        int rc = pthread_create(&thread, NULL, HandleTCPClient, (void *) clntSock);
        if(rc) { printf("Hilo no se pudo crear\n"); }
        else   { printf("Hilo creado exitosamente\n"); }
    }
}
